<?php

/* frontend/default/base.html.twig */
class __TwigTemplate_40b7302d2f3870d6a6d428138111ebdd8be7e6c057d6d8dc1032d99648ea001b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts_head' => array($this, 'block_javascripts_head'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_972e7106d886508aae92c775064029164cb9ee0ecf9f9b3528eee3b35bdcb74b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_972e7106d886508aae92c775064029164cb9ee0ecf9f9b3528eee3b35bdcb74b->enter($__internal_972e7106d886508aae92c775064029164cb9ee0ecf9f9b3528eee3b35bdcb74b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/base.html.twig"));

        $__internal_c70fdd4c2f9397bc608181a4d8b3a93adde389a4a216d288549f4e7aaa6f4c22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c70fdd4c2f9397bc608181a4d8b3a93adde389a4a216d288549f4e7aaa6f4c22->enter($__internal_c70fdd4c2f9397bc608181a4d8b3a93adde389a4a216d288549f4e7aaa6f4c22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">

        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        ";
        // line 11
        $this->displayBlock('javascripts_head', $context, $blocks);
        // line 12
        echo "    </head>
    <body>
        ";
        // line 14
        $this->loadTemplate("frontend/default/includes/header.html.twig", "frontend/default/base.html.twig", 14)->display($context);
        // line 15
        echo "
        <div class=\"container\">
            ";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 18
        echo "        </div>

        <script src=\"http://code.jquery.com/jquery-3.2.1.min.js\"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
        ";
        // line 23
        $this->displayBlock('javascripts', $context, $blocks);
        // line 24
        echo "    </body>
</html>
";
        
        $__internal_972e7106d886508aae92c775064029164cb9ee0ecf9f9b3528eee3b35bdcb74b->leave($__internal_972e7106d886508aae92c775064029164cb9ee0ecf9f9b3528eee3b35bdcb74b_prof);

        
        $__internal_c70fdd4c2f9397bc608181a4d8b3a93adde389a4a216d288549f4e7aaa6f4c22->leave($__internal_c70fdd4c2f9397bc608181a4d8b3a93adde389a4a216d288549f4e7aaa6f4c22_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_eb96353ead641eb0d288844006994fde2ade5c12b9dfb218198662b88c2a5cb7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb96353ead641eb0d288844006994fde2ade5c12b9dfb218198662b88c2a5cb7->enter($__internal_eb96353ead641eb0d288844006994fde2ade5c12b9dfb218198662b88c2a5cb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4444b4add9ac4602080e2b655be916b16e275fabfb2f6d41e31891fbbb13f54c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4444b4add9ac4602080e2b655be916b16e275fabfb2f6d41e31891fbbb13f54c->enter($__internal_4444b4add9ac4602080e2b655be916b16e275fabfb2f6d41e31891fbbb13f54c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_4444b4add9ac4602080e2b655be916b16e275fabfb2f6d41e31891fbbb13f54c->leave($__internal_4444b4add9ac4602080e2b655be916b16e275fabfb2f6d41e31891fbbb13f54c_prof);

        
        $__internal_eb96353ead641eb0d288844006994fde2ade5c12b9dfb218198662b88c2a5cb7->leave($__internal_eb96353ead641eb0d288844006994fde2ade5c12b9dfb218198662b88c2a5cb7_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_12d7aea9c965acdb19d6e0a4d49276b2ecb9b513881cb2b1da8576f8d611c352 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12d7aea9c965acdb19d6e0a4d49276b2ecb9b513881cb2b1da8576f8d611c352->enter($__internal_12d7aea9c965acdb19d6e0a4d49276b2ecb9b513881cb2b1da8576f8d611c352_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3b53e3713b17a80b02f636a695ee9da8b0ebaca033b58b80c7077b4b068c435d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b53e3713b17a80b02f636a695ee9da8b0ebaca033b58b80c7077b4b068c435d->enter($__internal_3b53e3713b17a80b02f636a695ee9da8b0ebaca033b58b80c7077b4b068c435d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3b53e3713b17a80b02f636a695ee9da8b0ebaca033b58b80c7077b4b068c435d->leave($__internal_3b53e3713b17a80b02f636a695ee9da8b0ebaca033b58b80c7077b4b068c435d_prof);

        
        $__internal_12d7aea9c965acdb19d6e0a4d49276b2ecb9b513881cb2b1da8576f8d611c352->leave($__internal_12d7aea9c965acdb19d6e0a4d49276b2ecb9b513881cb2b1da8576f8d611c352_prof);

    }

    // line 11
    public function block_javascripts_head($context, array $blocks = array())
    {
        $__internal_33bb8a94be7270e0e9642873a63578f87f537f5bb214e266f2ce3cea9a7cbb67 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33bb8a94be7270e0e9642873a63578f87f537f5bb214e266f2ce3cea9a7cbb67->enter($__internal_33bb8a94be7270e0e9642873a63578f87f537f5bb214e266f2ce3cea9a7cbb67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts_head"));

        $__internal_94afb25817a6786710447b04417aa960d95ca74484127e1f1dc804c2d8618712 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94afb25817a6786710447b04417aa960d95ca74484127e1f1dc804c2d8618712->enter($__internal_94afb25817a6786710447b04417aa960d95ca74484127e1f1dc804c2d8618712_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts_head"));

        echo " ";
        
        $__internal_94afb25817a6786710447b04417aa960d95ca74484127e1f1dc804c2d8618712->leave($__internal_94afb25817a6786710447b04417aa960d95ca74484127e1f1dc804c2d8618712_prof);

        
        $__internal_33bb8a94be7270e0e9642873a63578f87f537f5bb214e266f2ce3cea9a7cbb67->leave($__internal_33bb8a94be7270e0e9642873a63578f87f537f5bb214e266f2ce3cea9a7cbb67_prof);

    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        $__internal_c083d72313f5d23a2aff1ecd59630641e5016516ec6c707742a480a7557dd81e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c083d72313f5d23a2aff1ecd59630641e5016516ec6c707742a480a7557dd81e->enter($__internal_c083d72313f5d23a2aff1ecd59630641e5016516ec6c707742a480a7557dd81e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4bea72f3066e7ce1235ea8a4e4f8dba99f98eb8f2afca075f898cfa009c8607e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4bea72f3066e7ce1235ea8a4e4f8dba99f98eb8f2afca075f898cfa009c8607e->enter($__internal_4bea72f3066e7ce1235ea8a4e4f8dba99f98eb8f2afca075f898cfa009c8607e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_4bea72f3066e7ce1235ea8a4e4f8dba99f98eb8f2afca075f898cfa009c8607e->leave($__internal_4bea72f3066e7ce1235ea8a4e4f8dba99f98eb8f2afca075f898cfa009c8607e_prof);

        
        $__internal_c083d72313f5d23a2aff1ecd59630641e5016516ec6c707742a480a7557dd81e->leave($__internal_c083d72313f5d23a2aff1ecd59630641e5016516ec6c707742a480a7557dd81e_prof);

    }

    // line 23
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ead7a472ede5088c05c007e893f1fbf8f174095d39c053b89fc40da8caade136 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ead7a472ede5088c05c007e893f1fbf8f174095d39c053b89fc40da8caade136->enter($__internal_ead7a472ede5088c05c007e893f1fbf8f174095d39c053b89fc40da8caade136_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_8ec896e1e862b41ba8699f6207fda8f4f069de484898f636c6fa4a8341494eaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ec896e1e862b41ba8699f6207fda8f4f069de484898f636c6fa4a8341494eaf->enter($__internal_8ec896e1e862b41ba8699f6207fda8f4f069de484898f636c6fa4a8341494eaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_8ec896e1e862b41ba8699f6207fda8f4f069de484898f636c6fa4a8341494eaf->leave($__internal_8ec896e1e862b41ba8699f6207fda8f4f069de484898f636c6fa4a8341494eaf_prof);

        
        $__internal_ead7a472ede5088c05c007e893f1fbf8f174095d39c053b89fc40da8caade136->leave($__internal_ead7a472ede5088c05c007e893f1fbf8f174095d39c053b89fc40da8caade136_prof);

    }

    public function getTemplateName()
    {
        return "frontend/default/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 23,  138 => 17,  120 => 11,  103 => 9,  85 => 5,  73 => 24,  71 => 23,  64 => 18,  62 => 17,  58 => 15,  56 => 14,  52 => 12,  50 => 11,  45 => 10,  43 => 9,  36 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">

        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        {% block javascripts_head %} {% endblock %}
    </head>
    <body>
        {% include 'frontend/default/includes/header.html.twig' %}

        <div class=\"container\">
            {% block body %}{% endblock %}
        </div>

        <script src=\"http://code.jquery.com/jquery-3.2.1.min.js\"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "frontend/default/base.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/frontend/default/base.html.twig");
    }
}
