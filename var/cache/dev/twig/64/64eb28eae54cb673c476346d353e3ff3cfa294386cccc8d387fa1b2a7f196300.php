<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_5f7af2ab54397673e9c9a2278a33b5b524159de192473dffbfc27f4f3edcb23f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf31c090d0e3859f3fa5386d6d1ae83fef73bcdd69fe915226eafd9da4a5f535 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf31c090d0e3859f3fa5386d6d1ae83fef73bcdd69fe915226eafd9da4a5f535->enter($__internal_cf31c090d0e3859f3fa5386d6d1ae83fef73bcdd69fe915226eafd9da4a5f535_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_6c29feab8f7bdcb4850513ed645b8729c5f212c1f9969068da06e3a67ea30bd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c29feab8f7bdcb4850513ed645b8729c5f212c1f9969068da06e3a67ea30bd2->enter($__internal_6c29feab8f7bdcb4850513ed645b8729c5f212c1f9969068da06e3a67ea30bd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf31c090d0e3859f3fa5386d6d1ae83fef73bcdd69fe915226eafd9da4a5f535->leave($__internal_cf31c090d0e3859f3fa5386d6d1ae83fef73bcdd69fe915226eafd9da4a5f535_prof);

        
        $__internal_6c29feab8f7bdcb4850513ed645b8729c5f212c1f9969068da06e3a67ea30bd2->leave($__internal_6c29feab8f7bdcb4850513ed645b8729c5f212c1f9969068da06e3a67ea30bd2_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_f917d24d635459db7252d6c161e869a15cd7355bcdc1cac0e8f781d1e5c161bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f917d24d635459db7252d6c161e869a15cd7355bcdc1cac0e8f781d1e5c161bc->enter($__internal_f917d24d635459db7252d6c161e869a15cd7355bcdc1cac0e8f781d1e5c161bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_e0e37987ada0b9dc5f35cf4648e7822fda056802d26b6228c5a4ebcd082ab3d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0e37987ada0b9dc5f35cf4648e7822fda056802d26b6228c5a4ebcd082ab3d7->enter($__internal_e0e37987ada0b9dc5f35cf4648e7822fda056802d26b6228c5a4ebcd082ab3d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_e0e37987ada0b9dc5f35cf4648e7822fda056802d26b6228c5a4ebcd082ab3d7->leave($__internal_e0e37987ada0b9dc5f35cf4648e7822fda056802d26b6228c5a4ebcd082ab3d7_prof);

        
        $__internal_f917d24d635459db7252d6c161e869a15cd7355bcdc1cac0e8f781d1e5c161bc->leave($__internal_f917d24d635459db7252d6c161e869a15cd7355bcdc1cac0e8f781d1e5c161bc_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_549f7caf4276f18945611f15a58d8b6307168a4d4e782d49e098bb6ada3fbc36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_549f7caf4276f18945611f15a58d8b6307168a4d4e782d49e098bb6ada3fbc36->enter($__internal_549f7caf4276f18945611f15a58d8b6307168a4d4e782d49e098bb6ada3fbc36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_721db457a6f2943f7b24f3559ac76679f86a282d28011ea7e62d58377c985cd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_721db457a6f2943f7b24f3559ac76679f86a282d28011ea7e62d58377c985cd8->enter($__internal_721db457a6f2943f7b24f3559ac76679f86a282d28011ea7e62d58377c985cd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_721db457a6f2943f7b24f3559ac76679f86a282d28011ea7e62d58377c985cd8->leave($__internal_721db457a6f2943f7b24f3559ac76679f86a282d28011ea7e62d58377c985cd8_prof);

        
        $__internal_549f7caf4276f18945611f15a58d8b6307168a4d4e782d49e098bb6ada3fbc36->leave($__internal_549f7caf4276f18945611f15a58d8b6307168a4d4e782d49e098bb6ada3fbc36_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5d9c4c0d4749c573470bcd49059f0781e6f27bd7079218a29f0fc07db4eba17b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d9c4c0d4749c573470bcd49059f0781e6f27bd7079218a29f0fc07db4eba17b->enter($__internal_5d9c4c0d4749c573470bcd49059f0781e6f27bd7079218a29f0fc07db4eba17b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_18b75443c7d5a49140aa5f2e1c3007827b72ae28168c188ab9ad442a36105bc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18b75443c7d5a49140aa5f2e1c3007827b72ae28168c188ab9ad442a36105bc3->enter($__internal_18b75443c7d5a49140aa5f2e1c3007827b72ae28168c188ab9ad442a36105bc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_18b75443c7d5a49140aa5f2e1c3007827b72ae28168c188ab9ad442a36105bc3->leave($__internal_18b75443c7d5a49140aa5f2e1c3007827b72ae28168c188ab9ad442a36105bc3_prof);

        
        $__internal_5d9c4c0d4749c573470bcd49059f0781e6f27bd7079218a29f0fc07db4eba17b->leave($__internal_5d9c4c0d4749c573470bcd49059f0781e6f27bd7079218a29f0fc07db4eba17b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/geek/Bureau/Symfony-blog-master/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
