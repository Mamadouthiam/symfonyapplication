<?php

/* frontend/default/post/post.html.twig */
class __TwigTemplate_3053105160cd0042ff7b3b7d269b7f2d6190a7d0abfa7a7f9f72493f288405ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("frontend/default/base.html.twig", "frontend/default/post/post.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "frontend/default/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_663739e9d3b161b0008c1779de8ac15dcf51e9a113c7d273f7d1cf2ef94bc956 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_663739e9d3b161b0008c1779de8ac15dcf51e9a113c7d273f7d1cf2ef94bc956->enter($__internal_663739e9d3b161b0008c1779de8ac15dcf51e9a113c7d273f7d1cf2ef94bc956_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/post/post.html.twig"));

        $__internal_51610c92992e6b21ddc8f55f6e65763b7d09a45af80280e47e4e867069924205 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51610c92992e6b21ddc8f55f6e65763b7d09a45af80280e47e4e867069924205->enter($__internal_51610c92992e6b21ddc8f55f6e65763b7d09a45af80280e47e4e867069924205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/post/post.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_663739e9d3b161b0008c1779de8ac15dcf51e9a113c7d273f7d1cf2ef94bc956->leave($__internal_663739e9d3b161b0008c1779de8ac15dcf51e9a113c7d273f7d1cf2ef94bc956_prof);

        
        $__internal_51610c92992e6b21ddc8f55f6e65763b7d09a45af80280e47e4e867069924205->leave($__internal_51610c92992e6b21ddc8f55f6e65763b7d09a45af80280e47e4e867069924205_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_0724239a6a4d0ec5fad4510603e3077efbfa536f0725cff521d43674151fee30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0724239a6a4d0ec5fad4510603e3077efbfa536f0725cff521d43674151fee30->enter($__internal_0724239a6a4d0ec5fad4510603e3077efbfa536f0725cff521d43674151fee30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a27215a47fcb18ed45b5c94ae8099b834549b98a3b1f31593d4b2aeaa8b57c19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a27215a47fcb18ed45b5c94ae8099b834549b98a3b1f31593d4b2aeaa8b57c19->enter($__internal_a27215a47fcb18ed45b5c94ae8099b834549b98a3b1f31593d4b2aeaa8b57c19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-md-12\">
            <h4>
                ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "title", array()), "html", null, true);
        echo " <strong>(<i class=\"glyphicon glyphicon-signal\"></i> ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "viewsCount", array()), "html", null, true);
        echo ")</strong>
            </h4>
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    ";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "body", array()), "html", null, true);
        echo "
                </div>
            </div>
            <div class=\"text-right\">
                by <strong>
                    <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("user_posts", array("email" => $this->getAttribute($this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "user", array()), "email", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "user", array()), "name", array()), "html", null, true);
        echo "</a>
                </strong> at ";
        // line 17
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["post"] ?? $this->getContext($context, "post")), "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_a27215a47fcb18ed45b5c94ae8099b834549b98a3b1f31593d4b2aeaa8b57c19->leave($__internal_a27215a47fcb18ed45b5c94ae8099b834549b98a3b1f31593d4b2aeaa8b57c19_prof);

        
        $__internal_0724239a6a4d0ec5fad4510603e3077efbfa536f0725cff521d43674151fee30->leave($__internal_0724239a6a4d0ec5fad4510603e3077efbfa536f0725cff521d43674151fee30_prof);

    }

    public function getTemplateName()
    {
        return "frontend/default/post/post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 17,  71 => 16,  63 => 11,  54 => 7,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'frontend/default/base.html.twig' %}

{% block body %}
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h4>
                {{ post.title }} <strong>(<i class=\"glyphicon glyphicon-signal\"></i> {{ post.viewsCount }})</strong>
            </h4>
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">
                    {{ post.body }}
                </div>
            </div>
            <div class=\"text-right\">
                by <strong>
                    <a href=\"{{ url('user_posts', {\"email\": post.user.email}) }}\">{{ post.user.name }}</a>
                </strong> at {{ post.createdAt | date('Y-m-d H:i:s') }}
            </div>
        </div>
    </div>
{% endblock %}", "frontend/default/post/post.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/frontend/default/post/post.html.twig");
    }
}
