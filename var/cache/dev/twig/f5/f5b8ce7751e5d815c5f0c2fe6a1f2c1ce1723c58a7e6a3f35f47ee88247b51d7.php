<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_0a0ad4357e9b5335118adf979080b6236915100e93cbdf20b7b87ea674130fdd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_508e4f088d22a8d0882a56f96427fb8226cdd93c83321939c589c5fb47605b62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_508e4f088d22a8d0882a56f96427fb8226cdd93c83321939c589c5fb47605b62->enter($__internal_508e4f088d22a8d0882a56f96427fb8226cdd93c83321939c589c5fb47605b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_5eb092ec35b6accf239cd54f7a279e116d625b089e29f8e417cd845d2dbb8e02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5eb092ec35b6accf239cd54f7a279e116d625b089e29f8e417cd845d2dbb8e02->enter($__internal_5eb092ec35b6accf239cd54f7a279e116d625b089e29f8e417cd845d2dbb8e02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_508e4f088d22a8d0882a56f96427fb8226cdd93c83321939c589c5fb47605b62->leave($__internal_508e4f088d22a8d0882a56f96427fb8226cdd93c83321939c589c5fb47605b62_prof);

        
        $__internal_5eb092ec35b6accf239cd54f7a279e116d625b089e29f8e417cd845d2dbb8e02->leave($__internal_5eb092ec35b6accf239cd54f7a279e116d625b089e29f8e417cd845d2dbb8e02_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_93cac26462f5a9985bba6c195da7636fac560529dfce8f6179f2a0a2ddeae194 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93cac26462f5a9985bba6c195da7636fac560529dfce8f6179f2a0a2ddeae194->enter($__internal_93cac26462f5a9985bba6c195da7636fac560529dfce8f6179f2a0a2ddeae194_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_d55780c9b9f48e0d18509a69f1a7e47b8ab179c0c8a5b68beaed36391f5da3b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d55780c9b9f48e0d18509a69f1a7e47b8ab179c0c8a5b68beaed36391f5da3b5->enter($__internal_d55780c9b9f48e0d18509a69f1a7e47b8ab179c0c8a5b68beaed36391f5da3b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_d55780c9b9f48e0d18509a69f1a7e47b8ab179c0c8a5b68beaed36391f5da3b5->leave($__internal_d55780c9b9f48e0d18509a69f1a7e47b8ab179c0c8a5b68beaed36391f5da3b5_prof);

        
        $__internal_93cac26462f5a9985bba6c195da7636fac560529dfce8f6179f2a0a2ddeae194->leave($__internal_93cac26462f5a9985bba6c195da7636fac560529dfce8f6179f2a0a2ddeae194_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/geek/Bureau/Symfony-blog-master/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
