<?php

/* frontend/default/post/index.html.twig */
class __TwigTemplate_2bc0e339b7a68eb4879d997ca8bf4bee802ac8d5eef0d9370a8c540157926eac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("frontend/default/base.html.twig", "frontend/default/post/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "frontend/default/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56515fd63f7b6a5f60245d3d5830d89b09bbd1835f8db54f2d034684da609986 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56515fd63f7b6a5f60245d3d5830d89b09bbd1835f8db54f2d034684da609986->enter($__internal_56515fd63f7b6a5f60245d3d5830d89b09bbd1835f8db54f2d034684da609986_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/post/index.html.twig"));

        $__internal_c5721d5791e8d3f078f044aa6b886303bfededc1ba6adf88908de205b436f3e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5721d5791e8d3f078f044aa6b886303bfededc1ba6adf88908de205b436f3e3->enter($__internal_c5721d5791e8d3f078f044aa6b886303bfededc1ba6adf88908de205b436f3e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/post/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56515fd63f7b6a5f60245d3d5830d89b09bbd1835f8db54f2d034684da609986->leave($__internal_56515fd63f7b6a5f60245d3d5830d89b09bbd1835f8db54f2d034684da609986_prof);

        
        $__internal_c5721d5791e8d3f078f044aa6b886303bfededc1ba6adf88908de205b436f3e3->leave($__internal_c5721d5791e8d3f078f044aa6b886303bfededc1ba6adf88908de205b436f3e3_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_8ee6dd01492c94f4ebda956a90269d9aaf2b3069ff5fde8e122813b8d57eb8b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ee6dd01492c94f4ebda956a90269d9aaf2b3069ff5fde8e122813b8d57eb8b1->enter($__internal_8ee6dd01492c94f4ebda956a90269d9aaf2b3069ff5fde8e122813b8d57eb8b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d9ba497853d029d4a87efeb096cfa1c3a1af927f7cb313cab78e42d52023d626 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9ba497853d029d4a87efeb096cfa1c3a1af927f7cb313cab78e42d52023d626->enter($__internal_d9ba497853d029d4a87efeb096cfa1c3a1af927f7cb313cab78e42d52023d626_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-md-12\">

            <h4>
                Total Posts: <strong>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute(($context["paginator"] ?? $this->getContext($context, "paginator")), "getTotalItemCount", array()), "html", null, true);
        echo "</strong>
            </h4>

            ";
        // line 11
        $this->loadTemplate("messages/success.html.twig", "frontend/default/post/index.html.twig", 11)->display($context);
        // line 12
        echo "
            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["paginator"] ?? $this->getContext($context, "paginator")));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 14
            echo "                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post", array("id" => $this->getAttribute($context["post"], "id", array()), "slug" => $this->getAttribute($context["post"], "slug", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "title", array()), "html", null, true);
            echo "</a>
                    </div>
                    <div class=\"panel-body\">
                        ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["post"], "excerpt", array()), "html", null, true);
            echo "
                        <hr>
                        by <strong>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("user_posts", array("email" => $this->getAttribute($this->getAttribute($context["post"], "user", array()), "email", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["post"], "user", array()), "name", array()), "html", null, true);
            echo "</a>
                        </strong> at ";
            // line 23
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["post"], "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "
            <div class=\"text-center\">
                ";
        // line 29
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["paginator"] ?? $this->getContext($context, "paginator")));
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_d9ba497853d029d4a87efeb096cfa1c3a1af927f7cb313cab78e42d52023d626->leave($__internal_d9ba497853d029d4a87efeb096cfa1c3a1af927f7cb313cab78e42d52023d626_prof);

        
        $__internal_8ee6dd01492c94f4ebda956a90269d9aaf2b3069ff5fde8e122813b8d57eb8b1->leave($__internal_8ee6dd01492c94f4ebda956a90269d9aaf2b3069ff5fde8e122813b8d57eb8b1_prof);

    }

    public function getTemplateName()
    {
        return "frontend/default/post/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 29,  104 => 27,  94 => 23,  88 => 22,  82 => 19,  74 => 16,  70 => 14,  66 => 13,  63 => 12,  61 => 11,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'frontend/default/base.html.twig' %}

{% block body %}
    <div class=\"row\">
        <div class=\"col-md-12\">

            <h4>
                Total Posts: <strong>{{ paginator.getTotalItemCount }}</strong>
            </h4>

            {% include 'messages/success.html.twig' %}

            {% for post in paginator %}
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <a href=\"{{ path('post', {'id':post.id,'slug': post.slug}) }}\">{{ post.title }}</a>
                    </div>
                    <div class=\"panel-body\">
                        {{ post.excerpt }}
                        <hr>
                        by <strong>
                            <a href=\"{{ url('user_posts', {\"email\": post.user.email}) }}\">{{ post.user.name }}</a>
                        </strong> at {{ post.createdAt | date('Y-m-d H:i:s') }}
                    </div>
                </div>
            {% endfor %}

            <div class=\"text-center\">
                {{ knp_pagination_render(paginator) }}
            </div>
        </div>
    </div>
{% endblock %}
", "frontend/default/post/index.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/frontend/default/post/index.html.twig");
    }
}
