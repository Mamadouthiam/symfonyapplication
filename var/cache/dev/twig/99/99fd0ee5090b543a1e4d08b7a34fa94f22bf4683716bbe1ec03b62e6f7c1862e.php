<?php

/* auth/login/index.html.twig */
class __TwigTemplate_0c085ea2219ac3bc26aafea06562091f6b94dd1b0f5411f465aba20d7525c3d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate(":auth:base.html.twig", "auth/login/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return ":auth:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_06913523555e3fdb94d5deda48b07134bc252e8dcaec5462388e7af781255223 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_06913523555e3fdb94d5deda48b07134bc252e8dcaec5462388e7af781255223->enter($__internal_06913523555e3fdb94d5deda48b07134bc252e8dcaec5462388e7af781255223_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "auth/login/index.html.twig"));

        $__internal_9f2d68cfa5ddb05c511810851f3742bf8a12be113fa40ab5075efb35caf3ef8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f2d68cfa5ddb05c511810851f3742bf8a12be113fa40ab5075efb35caf3ef8a->enter($__internal_9f2d68cfa5ddb05c511810851f3742bf8a12be113fa40ab5075efb35caf3ef8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "auth/login/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_06913523555e3fdb94d5deda48b07134bc252e8dcaec5462388e7af781255223->leave($__internal_06913523555e3fdb94d5deda48b07134bc252e8dcaec5462388e7af781255223_prof);

        
        $__internal_9f2d68cfa5ddb05c511810851f3742bf8a12be113fa40ab5075efb35caf3ef8a->leave($__internal_9f2d68cfa5ddb05c511810851f3742bf8a12be113fa40ab5075efb35caf3ef8a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_324e46ca34cbd7df48f8689ec5c958d52e06b2da17b2883e387d22eccc902e61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_324e46ca34cbd7df48f8689ec5c958d52e06b2da17b2883e387d22eccc902e61->enter($__internal_324e46ca34cbd7df48f8689ec5c958d52e06b2da17b2883e387d22eccc902e61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_296493483e32e7769022e52fea36cf17f8acbeb15081128b411c760c6c3ebe0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_296493483e32e7769022e52fea36cf17f8acbeb15081128b411c760c6c3ebe0b->enter($__internal_296493483e32e7769022e52fea36cf17f8acbeb15081128b411c760c6c3ebe0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"c-margin-t-80\">
        <div class=\"row\">
            <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        Login
                    </div>
                    <div class=\"panel-body\">
                        ";
        // line 12
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 13
            echo "                            <div class=\"alert alert-danger\">
                                <p>";
            // line 14
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        // line 17
        echo "                        <form action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"POST\">
                            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\">
                            <div class=\"form-group\">
                                <label for=\"\">Username</label>
                                <input type=\"text\" name=\"email\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, ($context["lastUsername"] ?? $this->getContext($context, "lastUsername")), "html", null, true);
        echo "\" class=\"form-control\">
                            </div>
                            <div class=\"form-group\">
                                <label for=\"\">Password</label>
                                <input type=\"password\" name=\"password\" class=\"form-control\">
                            </div>
                            <div class=\"form-group\">
                                <input type=\"submit\" value=\"Login\" class=\"btn btn-default button-block\">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_296493483e32e7769022e52fea36cf17f8acbeb15081128b411c760c6c3ebe0b->leave($__internal_296493483e32e7769022e52fea36cf17f8acbeb15081128b411c760c6c3ebe0b_prof);

        
        $__internal_324e46ca34cbd7df48f8689ec5c958d52e06b2da17b2883e387d22eccc902e61->leave($__internal_324e46ca34cbd7df48f8689ec5c958d52e06b2da17b2883e387d22eccc902e61_prof);

    }

    public function getTemplateName()
    {
        return "auth/login/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 21,  75 => 18,  70 => 17,  64 => 14,  61 => 13,  59 => 12,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends ':auth:base.html.twig' %}

{% block body %}
    <div class=\"c-margin-t-80\">
        <div class=\"row\">
            <div class=\"col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        Login
                    </div>
                    <div class=\"panel-body\">
                        {% if error %}
                            <div class=\"alert alert-danger\">
                                <p>{{ error }}</p>
                            </div>
                        {% endif %}
                        <form action=\"{{ path('login') }}\" method=\"POST\">
                            <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\">
                            <div class=\"form-group\">
                                <label for=\"\">Username</label>
                                <input type=\"text\" name=\"email\" value=\"{{ lastUsername }}\" class=\"form-control\">
                            </div>
                            <div class=\"form-group\">
                                <label for=\"\">Password</label>
                                <input type=\"password\" name=\"password\" class=\"form-control\">
                            </div>
                            <div class=\"form-group\">
                                <input type=\"submit\" value=\"Login\" class=\"btn btn-default button-block\">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}", "auth/login/index.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/auth/login/index.html.twig");
    }
}
