<?php

/* frontend/default/includes/header.html.twig */
class __TwigTemplate_e1eca70d141e28cb5d1e4dfabfae6fa48bcdbda53c59a500d180ac7381c6b1f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c74b270d7e5b40e3009154d3c6ab69e7be75838da8a12dda195dd16145684538 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c74b270d7e5b40e3009154d3c6ab69e7be75838da8a12dda195dd16145684538->enter($__internal_c74b270d7e5b40e3009154d3c6ab69e7be75838da8a12dda195dd16145684538_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/includes/header.html.twig"));

        $__internal_a1ad3a52bea120b684b575267fc2c94d62ddae399e715750ad90bd17d18ba2f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1ad3a52bea120b684b575267fc2c94d62ddae399e715750ad90bd17d18ba2f6->enter($__internal_a1ad3a52bea120b684b575267fc2c94d62ddae399e715750ad90bd17d18ba2f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/includes/header.html.twig"));

        // line 1
        echo "<nav class=\"navbar navbar-default\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("homepage");
        echo "\">Blog</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\"
                       aria-expanded=\"false\">
                        Users <span class=\"caret\"></span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("users");
        echo "\">Users List</a></li>
                    </ul>
                </li>
            </ul>

            ";
        // line 29
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 30
            echo "                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\"
                           aria-expanded=\"false\">
                            ";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "name", array()), "html", null, true);
            echo " <span class=\"caret\"></span>
                        </a>
                        <ul class=\"dropdown-menu\">
                            <li><a href=\"";
            // line 37
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("user_profile");
            echo "\">Profile</a></li>
                            <li><a href=\"";
            // line 38
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("user_create_post");
            echo "\">Create Post</a></li>
                            <li><a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("user_posts", array("email" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "email", array()))), "html", null, true);
            echo "\">Posts</a></li>
                            <li><a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("logout", array("_token" => $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderCsrfToken("logout"))), "html", null, true);
            echo "\">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            ";
        } else {
            // line 45
            echo "                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\"
                           aria-expanded=\"false\">
                            User <span class=\"caret\"></span>
                        </a>
                        <ul class=\"dropdown-menu\">
                            <li><a href=\"";
            // line 52
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("login");
            echo "\">Login</a></li>
                        </ul>
                    </li>
                </ul>
            ";
        }
        // line 57
        echo "
        </div>
    </div>
</nav>";
        
        $__internal_c74b270d7e5b40e3009154d3c6ab69e7be75838da8a12dda195dd16145684538->leave($__internal_c74b270d7e5b40e3009154d3c6ab69e7be75838da8a12dda195dd16145684538_prof);

        
        $__internal_a1ad3a52bea120b684b575267fc2c94d62ddae399e715750ad90bd17d18ba2f6->leave($__internal_a1ad3a52bea120b684b575267fc2c94d62ddae399e715750ad90bd17d18ba2f6_prof);

    }

    public function getTemplateName()
    {
        return "frontend/default/includes/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 57,  104 => 52,  95 => 45,  87 => 40,  83 => 39,  79 => 38,  75 => 37,  69 => 34,  63 => 30,  61 => 29,  53 => 24,  38 => 12,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<nav class=\"navbar navbar-default\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"{{ url('homepage') }}\">Blog</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\"
                       aria-expanded=\"false\">
                        Users <span class=\"caret\"></span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"{{ url('users') }}\">Users List</a></li>
                    </ul>
                </li>
            </ul>

            {% if app.user %}
                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\"
                           aria-expanded=\"false\">
                            {{ app.user.name }} <span class=\"caret\"></span>
                        </a>
                        <ul class=\"dropdown-menu\">
                            <li><a href=\"{{ url('user_profile') }}\">Profile</a></li>
                            <li><a href=\"{{ url('user_create_post') }}\">Create Post</a></li>
                            <li><a href=\"{{ url('user_posts', {\"email\": app.user.email}) }}\">Posts</a></li>
                            <li><a href=\"{{ url('logout', {'_token': csrf_token('logout')}) }}\">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            {% else %}
                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\"
                           aria-expanded=\"false\">
                            User <span class=\"caret\"></span>
                        </a>
                        <ul class=\"dropdown-menu\">
                            <li><a href=\"{{ url('login') }}\">Login</a></li>
                        </ul>
                    </li>
                </ul>
            {% endif %}

        </div>
    </div>
</nav>", "frontend/default/includes/header.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/frontend/default/includes/header.html.twig");
    }
}
