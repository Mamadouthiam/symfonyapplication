<?php

/* messages/success.html.twig */
class __TwigTemplate_137829b450ee45dd2f0ddac24dabc4070247ccf54d860832dff2a17347f3c7d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c820a4ab286c36ce54b6444a7fb1bb5a45382678e3fcce3429742b1e5ab8f71f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c820a4ab286c36ce54b6444a7fb1bb5a45382678e3fcce3429742b1e5ab8f71f->enter($__internal_c820a4ab286c36ce54b6444a7fb1bb5a45382678e3fcce3429742b1e5ab8f71f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "messages/success.html.twig"));

        $__internal_f97b3e9e327e7f4055eba18586a6b8252784c39275cf693ff24931d51a84b3a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f97b3e9e327e7f4055eba18586a6b8252784c39275cf693ff24931d51a84b3a1->enter($__internal_f97b3e9e327e7f4055eba18586a6b8252784c39275cf693ff24931d51a84b3a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "messages/success.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "    <div class=\"alert alert-success\">
        <p>";
            // line 3
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "</p>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_c820a4ab286c36ce54b6444a7fb1bb5a45382678e3fcce3429742b1e5ab8f71f->leave($__internal_c820a4ab286c36ce54b6444a7fb1bb5a45382678e3fcce3429742b1e5ab8f71f_prof);

        
        $__internal_f97b3e9e327e7f4055eba18586a6b8252784c39275cf693ff24931d51a84b3a1->leave($__internal_f97b3e9e327e7f4055eba18586a6b8252784c39275cf693ff24931d51a84b3a1_prof);

    }

    public function getTemplateName()
    {
        return "messages/success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 3,  29 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% for message in app.session.flashBag.get('success') %}
    <div class=\"alert alert-success\">
        <p>{{ message }}</p>
    </div>
{% endfor %}", "messages/success.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/messages/success.html.twig");
    }
}
