<?php

/* :auth:base.html.twig */
class __TwigTemplate_0a96b625f1f797b9519da4b3031010c07a8aa9533bed6308265aa41360f0c760 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df398892076e9618419d169a72acb7778e3962a0a9e4414e3a3fdacdb15cde39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df398892076e9618419d169a72acb7778e3962a0a9e4414e3a3fdacdb15cde39->enter($__internal_df398892076e9618419d169a72acb7778e3962a0a9e4414e3a3fdacdb15cde39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":auth:base.html.twig"));

        $__internal_b598ed457291cfa5f0100a84472c487d864f244db699cd67c40e25844f886f16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b598ed457291cfa5f0100a84472c487d864f244db699cd67c40e25844f886f16->enter($__internal_b598ed457291cfa5f0100a84472c487d864f244db699cd67c40e25844f886f16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":auth:base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/styles/app.css"), "html", null, true);
        echo "\">
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        <div class=\"container\">
            ";
        // line 15
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "        </div>

        <script src=\"http://code.jquery.com/jquery-3.2.1.min.js\"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
        ";
        // line 21
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "    </body>
</html>
";
        
        $__internal_df398892076e9618419d169a72acb7778e3962a0a9e4414e3a3fdacdb15cde39->leave($__internal_df398892076e9618419d169a72acb7778e3962a0a9e4414e3a3fdacdb15cde39_prof);

        
        $__internal_b598ed457291cfa5f0100a84472c487d864f244db699cd67c40e25844f886f16->leave($__internal_b598ed457291cfa5f0100a84472c487d864f244db699cd67c40e25844f886f16_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_967dc2d96f1eb21a1f9f10051aebde2a4b22b77a874e31c30919873195e82fce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_967dc2d96f1eb21a1f9f10051aebde2a4b22b77a874e31c30919873195e82fce->enter($__internal_967dc2d96f1eb21a1f9f10051aebde2a4b22b77a874e31c30919873195e82fce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_47e49d9084ea26f9247aca5edef373c4a9574ef1e872aee112b041cf4763c9a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47e49d9084ea26f9247aca5edef373c4a9574ef1e872aee112b041cf4763c9a2->enter($__internal_47e49d9084ea26f9247aca5edef373c4a9574ef1e872aee112b041cf4763c9a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_47e49d9084ea26f9247aca5edef373c4a9574ef1e872aee112b041cf4763c9a2->leave($__internal_47e49d9084ea26f9247aca5edef373c4a9574ef1e872aee112b041cf4763c9a2_prof);

        
        $__internal_967dc2d96f1eb21a1f9f10051aebde2a4b22b77a874e31c30919873195e82fce->leave($__internal_967dc2d96f1eb21a1f9f10051aebde2a4b22b77a874e31c30919873195e82fce_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_793407d6e1cd2e0346293a4d9b78f4029e4cf536d9870ae12a3272b9b94d555d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_793407d6e1cd2e0346293a4d9b78f4029e4cf536d9870ae12a3272b9b94d555d->enter($__internal_793407d6e1cd2e0346293a4d9b78f4029e4cf536d9870ae12a3272b9b94d555d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_dcc36ee6adaee3387b46a80ec785946570ccd5e797a652578d44b9ecf40264d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcc36ee6adaee3387b46a80ec785946570ccd5e797a652578d44b9ecf40264d7->enter($__internal_dcc36ee6adaee3387b46a80ec785946570ccd5e797a652578d44b9ecf40264d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_dcc36ee6adaee3387b46a80ec785946570ccd5e797a652578d44b9ecf40264d7->leave($__internal_dcc36ee6adaee3387b46a80ec785946570ccd5e797a652578d44b9ecf40264d7_prof);

        
        $__internal_793407d6e1cd2e0346293a4d9b78f4029e4cf536d9870ae12a3272b9b94d555d->leave($__internal_793407d6e1cd2e0346293a4d9b78f4029e4cf536d9870ae12a3272b9b94d555d_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_9af8982b726c7d1115bfe07eb9bcb3ebebb236fa8e1827ff07500b556da18f3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9af8982b726c7d1115bfe07eb9bcb3ebebb236fa8e1827ff07500b556da18f3d->enter($__internal_9af8982b726c7d1115bfe07eb9bcb3ebebb236fa8e1827ff07500b556da18f3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_81f98027802383a7368292f399fadd1b01020f586da4ceaa70883a878b1b89cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81f98027802383a7368292f399fadd1b01020f586da4ceaa70883a878b1b89cf->enter($__internal_81f98027802383a7368292f399fadd1b01020f586da4ceaa70883a878b1b89cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_81f98027802383a7368292f399fadd1b01020f586da4ceaa70883a878b1b89cf->leave($__internal_81f98027802383a7368292f399fadd1b01020f586da4ceaa70883a878b1b89cf_prof);

        
        $__internal_9af8982b726c7d1115bfe07eb9bcb3ebebb236fa8e1827ff07500b556da18f3d->leave($__internal_9af8982b726c7d1115bfe07eb9bcb3ebebb236fa8e1827ff07500b556da18f3d_prof);

    }

    // line 21
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0cbe42da1f5041909c077664eb33932df98283e45aba5918026bbe5d71557dd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cbe42da1f5041909c077664eb33932df98283e45aba5918026bbe5d71557dd2->enter($__internal_0cbe42da1f5041909c077664eb33932df98283e45aba5918026bbe5d71557dd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_8b94d4e2d4f27a03b7aa9cfab26265a81da7b557b63ef18b7404c6349a19e9bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b94d4e2d4f27a03b7aa9cfab26265a81da7b557b63ef18b7404c6349a19e9bf->enter($__internal_8b94d4e2d4f27a03b7aa9cfab26265a81da7b557b63ef18b7404c6349a19e9bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_8b94d4e2d4f27a03b7aa9cfab26265a81da7b557b63ef18b7404c6349a19e9bf->leave($__internal_8b94d4e2d4f27a03b7aa9cfab26265a81da7b557b63ef18b7404c6349a19e9bf_prof);

        
        $__internal_0cbe42da1f5041909c077664eb33932df98283e45aba5918026bbe5d71557dd2->leave($__internal_0cbe42da1f5041909c077664eb33932df98283e45aba5918026bbe5d71557dd2_prof);

    }

    public function getTemplateName()
    {
        return ":auth:base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 21,  114 => 15,  97 => 9,  79 => 5,  67 => 22,  65 => 21,  58 => 16,  56 => 15,  47 => 10,  45 => 9,  41 => 8,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"{{ asset('/styles/app.css') }}\">
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>

        <div class=\"container\">
            {% block body %}{% endblock %}
        </div>

        <script src=\"http://code.jquery.com/jquery-3.2.1.min.js\"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
        {% block javascripts %}{% endblock %}
    </body>
</html>
", ":auth:base.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/auth/base.html.twig");
    }
}
