<?php

/* frontend/default/users/index.html.twig */
class __TwigTemplate_886a17aea8a78eb7482ca94e1ac67443d77a9e968be842eee65f1d6c8c4c4b79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("frontend/default/base.html.twig", "frontend/default/users/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "frontend/default/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98612431dc4705a705d70f0f41436bfdaa05649e250c373aebba788fbdc4a93f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_98612431dc4705a705d70f0f41436bfdaa05649e250c373aebba788fbdc4a93f->enter($__internal_98612431dc4705a705d70f0f41436bfdaa05649e250c373aebba788fbdc4a93f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/users/index.html.twig"));

        $__internal_7f0063be87282de53c5a75ffb6129e8d925b9bfadfb854ee7fd05977d046cf23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f0063be87282de53c5a75ffb6129e8d925b9bfadfb854ee7fd05977d046cf23->enter($__internal_7f0063be87282de53c5a75ffb6129e8d925b9bfadfb854ee7fd05977d046cf23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "frontend/default/users/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_98612431dc4705a705d70f0f41436bfdaa05649e250c373aebba788fbdc4a93f->leave($__internal_98612431dc4705a705d70f0f41436bfdaa05649e250c373aebba788fbdc4a93f_prof);

        
        $__internal_7f0063be87282de53c5a75ffb6129e8d925b9bfadfb854ee7fd05977d046cf23->leave($__internal_7f0063be87282de53c5a75ffb6129e8d925b9bfadfb854ee7fd05977d046cf23_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4f0b0a5671d9afcf56a28a43a3033eaa91478830f9da0c22372cbd2728f00ad2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f0b0a5671d9afcf56a28a43a3033eaa91478830f9da0c22372cbd2728f00ad2->enter($__internal_4f0b0a5671d9afcf56a28a43a3033eaa91478830f9da0c22372cbd2728f00ad2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0468b0060bd3c48d0e0dad9500e9086dd3e098fb7a0d1eab295c7aa0a3a48eb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0468b0060bd3c48d0e0dad9500e9086dd3e098fb7a0d1eab295c7aa0a3a48eb1->enter($__internal_0468b0060bd3c48d0e0dad9500e9086dd3e098fb7a0d1eab295c7aa0a3a48eb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"row\">
        <div class=\"col-md-12\">

            <h4 class=\"text-center\">Users</h4>
            <table class=\"table table-striped table-responsive\">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Role</td>
                    <td>Published Posts</td>
                </tr>
                </thead>
                <tbody>
                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 21
            echo "                    <tr>
                        <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</td>
                        <td><a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_posts", array("email" => $this->getAttribute($context["user"], "email", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "name", array()), "html", null, true);
            echo "</a></td>
                        <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                        <td>
                            ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["user"], "roles", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 27
                echo "                                ";
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, twig_replace_filter($context["role"], array("ROLE_" => ""))), "html", null, true);
                echo "
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "                        </td>
                        <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["user"], "posts", array()), "count", array()), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                </tbody>
            </table>

        </div>
    </div>

";
        
        $__internal_0468b0060bd3c48d0e0dad9500e9086dd3e098fb7a0d1eab295c7aa0a3a48eb1->leave($__internal_0468b0060bd3c48d0e0dad9500e9086dd3e098fb7a0d1eab295c7aa0a3a48eb1_prof);

        
        $__internal_4f0b0a5671d9afcf56a28a43a3033eaa91478830f9da0c22372cbd2728f00ad2->leave($__internal_4f0b0a5671d9afcf56a28a43a3033eaa91478830f9da0c22372cbd2728f00ad2_prof);

    }

    public function getTemplateName()
    {
        return "frontend/default/users/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 33,  105 => 30,  102 => 29,  93 => 27,  89 => 26,  84 => 24,  78 => 23,  74 => 22,  71 => 21,  67 => 20,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'frontend/default/base.html.twig' %}

{% block body %}

    <div class=\"row\">
        <div class=\"col-md-12\">

            <h4 class=\"text-center\">Users</h4>
            <table class=\"table table-striped table-responsive\">
                <thead>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Role</td>
                    <td>Published Posts</td>
                </tr>
                </thead>
                <tbody>
                {% for user in users %}
                    <tr>
                        <td>{{ user.id }}</td>
                        <td><a href=\"{{ path('user_posts', {'email':user.email}) }}\">{{ user.name }}</a></td>
                        <td>{{ user.email }}</td>
                        <td>
                            {% for role in user.roles %}
                                {{ role|replace({'ROLE_': ''})|title }}
                            {% endfor %}
                        </td>
                        <td>{{ user.posts.count }}</td>
                    </tr>
                {% endfor %}
                </tbody>
            </table>

        </div>
    </div>

{% endblock %}", "frontend/default/users/index.html.twig", "/home/geek/Bureau/Symfony-blog-master/app/Resources/views/frontend/default/users/index.html.twig");
    }
}
